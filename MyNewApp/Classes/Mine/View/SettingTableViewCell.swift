//
//  SettingTableViewCell.swift
//  MyNewApp
//
//  Created by xyg on 2021/7/6.
//

import UIKit

class SettingTableViewCell: UITableViewCell,RegisterCellFromNib{

    @IBOutlet weak var rightlabel: UILabel!
    @IBOutlet weak var `switch`: UISwitch!
    @IBOutlet weak var titlelabel: UILabel!
    
    @IBOutlet weak var detaillabel: UILabel!
    
    @IBOutlet weak var arrowimg: UIImageView!
    
    var settinfmodel:settingModel?{
        didSet{
            self.titlelabel.text = settinfmodel?.title
            self.detaillabel.text = settinfmodel?.detailtitle
            self.rightlabel.text = settinfmodel?.righttitle
            self.arrowimg.isHidden = !settinfmodel!.isarrow
            self.switch.isHidden = !settinfmodel!.isswitch
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
