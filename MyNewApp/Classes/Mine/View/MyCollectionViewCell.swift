//
//  MyCollectionViewCell.swift
//  MyNewApp
//
//  Created by xyg on 2021/7/3.
//

import UIKit
import Kingfisher
class MyCollectionViewCell: UICollectionViewCell,RegisterCellFromNib{

    @IBOutlet weak var avaimage: UIImageView!
    
    @IBOutlet weak var namelabel: UILabel!
    
    @IBOutlet weak var vipimage: UIImageView!
    
    @IBOutlet weak var tips: UIImageView!
    
    var concernModel = MyConcern(){
        didSet{
            avaimage.kf.setImage(with: URL(string: concernModel.icon!))
            vipimage.isHidden = !concernModel.is_verify!
            vipimage.image = concernModel.userAuthInfo?.auth_type == 1 ? UIImage(named: "all_v_avatar_star_16x16_") : UIImage(named: "all_v_avatar_18x18_")
            tips.isHidden = !concernModel.tips!
            namelabel.text = concernModel.name
        }
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    

}
