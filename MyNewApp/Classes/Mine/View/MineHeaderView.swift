//
//  MineHeaderView.swift
//  MyNewApp
//
//  Created by xyg on 2021/7/3.
//

import UIKit
import IBAnimatable
import SwiftTheme
class MineHeaderView: UIView {

    //背景图片
    @IBOutlet weak var bgImageView: UIImageView!
    
    @IBOutlet weak var mobileButton: UIButton!
    
    @IBOutlet weak var wechatButton: UIButton!
    
    @IBOutlet weak var qqButton: UIButton!
    
    @IBOutlet weak var sinaButton: UIButton!
    
    @IBOutlet weak var faviriteButton: UIButton!
    
    @IBOutlet weak var dayNightButton: UIButton!
    
    @IBOutlet weak var historybutton: UIButton!
    
    @IBOutlet weak var moreButton: AnimatableButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //获取存储状态
       
        dayNightButton.isSelected = UserDefaults.standard.bool(forKey: isNight)
            
        dayNightButton.setTitle("夜间", for: .normal)
        dayNightButton.setTitle("日间", for: .selected)
        
        //主题颜色
        mobileButton.theme_setImage("images.me_header_mobilebutton", forState: .normal)
        wechatButton.theme_setImage("images.me_header_wechatbutton", forState: .normal)
        qqButton.theme_setImage("images.me_header_qqbutton", forState: .normal)
        sinaButton.theme_setImage("images.me_header_sinabutton", forState: .normal)
        faviriteButton.theme_setImage("images.me_header_favirite", forState: .normal)
        historybutton.theme_setImage("images.me_header_history", forState: .normal)
        dayNightButton.theme_setImage("images.me_header_daynight", forState: .normal)
        moreButton.theme_backgroundColor  = "colors.me_header_more_back"
        moreButton.theme_setTitleColor("colors.me_header_more_titlecolor", forState: .normal)
    }
    
    class func HeaderView() -> MineHeaderView {
        return Bundle.main.loadNibNamed("\(self)", owner: nil, options: nil)?.last as! MineHeaderView
    }
    
    

    @IBAction func dayOrnightClick(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        Mytheme.switchToNight(sender.isSelected)
        //将设置的夜间状态存储下来
        UserDefaults.standard.set(sender.isSelected, forKey: isNight)
        
        //当主题改变时，向tabbar 发送一条通知，去改变主题
        
        NotificationCenter.default.post(name:NSNotification.Name(rawValue: "dayOrnightClick"), object: sender.isSelected)
        
    }
    
}
