//
//  MyConcernTableViewCell.swift
//  MyNewApp
//
//  Created by xyg on 2021/7/3.
//

import UIKit


class MyConcernTableViewCell: UITableViewCell, RegisterCellFromNib{
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var rightLabel: UILabel!
    
    @IBOutlet weak var img: UIImageView!
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    

    //拿到关注数据数组
    var myConcerns = [MyConcern](){
        didSet{
            self.collectionView.reloadData()
        }
    }
    
    
    
    //
    var mycell:MyCellModel?{
        didSet{
            titleLabel.text = mycell?.text
            rightLabel.text = mycell?.grey_text
        }
    }
    
    
    
    //只需要关注一个用户的时候需要去设置
//    var myconcern:MyConcern?{
//        didSet{
//            print(myconcern)
//        }
//    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.collectionViewLayout = MycollectionViewFlowLayout()
        collectionView.delegate = self
        collectionView.dataSource  = self
        collectionView.ym_registerCell(cell: MyCollectionViewCell.self)
    }
    

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   
    
}

extension MyConcernTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return myConcerns.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.ym_dequeueReusableCell(indexPath: indexPath) as MyCollectionViewCell
        let myconcern = myConcerns[indexPath.row]
        cell.concernModel = myconcern
        print(myconcern.icon)
        return cell
    }
    
    
}


class MycollectionViewFlowLayout: UICollectionViewFlowLayout {
    override func prepare() {
        itemSize = CGSize(width: 58, height: 74)
        minimumLineSpacing = 0
        minimumLineSpacing = 0
        sectionInset = UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
        scrollDirection = .horizontal
    }
}


