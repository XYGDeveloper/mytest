//
//  MineOtherTableViewCell.swift
//  MyNewApp
//
//  Created by xyg on 2021/7/3.
//

import UIKit

class MineOtherTableViewCell: UITableViewCell,RegisterCellFromNib{

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var detailButton: UIImageView!
    
    @IBOutlet weak var rightLabel: UILabel!
    
    //设置model设置数据
//    var myCellModel:MyCellModel?{
//        didSet{
//            titleLabel.text = myCellModel?.text
//            rightLabel.text = myCellModel?.grey_text
//        }
//    }
    
    var myCellModel = MyCellModel(){
        didSet{
            titleLabel.text = myCellModel.text
            rightLabel.text = myCellModel.grey_text
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
