//
//  MineViewController.swift
//  MyNewApp
//
//  Created by xyg on 2021/7/2.
//

import UIKit
import RxSwift
import RxCocoa
class MineViewController: UITableViewController{

    var sections = [[MyCellModel]]()
    
    var myconcerns = [MyConcern]()
    
    var disposeBag = DisposeBag()
    
    
     lazy var headerView: MineHeaderView = {
        let header = MineHeaderView.HeaderView()
        return header
    }()
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    
    //状态栏的修改
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = UIColor.globalBackgroundColor()
        tableView.separatorStyle = .none
        tableView.tableHeaderView = headerView
//        tableView.register(UINib(nibName: String(describing: MyConcernTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: MyConcernTableViewCell.self))
//        tableView.register(UINib(nibName: String(describing: MineOtherTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: MineOtherTableViewCell.self))
        tableView.ym_registerCell(cell: MyConcernTableViewCell.self)
        tableView.ym_registerCell(cell: MineOtherTableViewCell.self)
        NetWorkTool.loadMycellData { sections in
            let jsonString = "{\"text\":\"我的关注\",\"gray_text\":\"\"}"
            let myConcern = MyCellModel.deserialize(from: jsonString)
            var myconcerns = [MyCellModel]()
            myconcerns.append(myConcern!)
            self.sections.append(myconcerns)
            self.sections += sections
            self.tableView.reloadData()
            NetWorkTool.loadMyconcernData { myconcerns in
                print(myconcerns.count)
                self.myconcerns = myconcerns
                let indexSet = IndexSet(integer: 0)
                self.tableView.reloadSections(indexSet, with: .automatic)
            }
        }
        
        /// 更多按钮点击
        headerView.moreButton.rx.tap
            .subscribe(onNext: { [weak self] in
                let moreLoginVC = LoginViewController.loadStoryboard()
                moreLoginVC.modalSize = (width: .full, height: .custom(size: Float(screenHeight - (isIPhoneX ? 44 : 20))))
                self!.present(moreLoginVC, animated: true, completion: nil)
            })
            .disposed(by: disposeBag)

    }
    
}

extension MineViewController{
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    // 每组头部的高度
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 1 ? 0 : 10
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row == 0 {
            return (myconcerns.count == 0 || myconcerns.count == 1) ? 40 : 146
        }
        return 40
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 && indexPath.row == 0 {
            let cell:MyConcernTableViewCell = tableView.ym_dequeueReusableCell(indexPath: indexPath)
            let section = sections[indexPath.section]
            let myCellModel:MyCellModel = section[indexPath.row]
           
//            if myconcerns.count == 1 {
//                cell.myconcern = myconcerns[0]
//            }

            if myconcerns.count == 0 || myconcerns.count == 1 {
                cell.collectionView.isHidden = true
                cell.mycell = myCellModel
            }
            
            if myconcerns.count > 1 {
                cell.myConcerns = myconcerns
            }
            
            cell.titleLabel.text = myCellModel.text
            cell.rightLabel.text = myCellModel.grey_text

            return cell
        }
        
        
        let cell:MineOtherTableViewCell = tableView.ym_dequeueReusableCell(indexPath: indexPath)
        let section = sections[indexPath.section]
        let myCellModel:MyCellModel = section[indexPath.row]
        cell.myCellModel = myCellModel
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 10))
        view.backgroundColor = UIColor(r: 247, g: 248, b: 259, alpha: 1.0)
        return view
    }
   
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 3 && indexPath.row == 1 {
            let setting = SettingTableViewController()
            setting.title = "设置"
            navigationController?.pushViewController(setting, animated: true)
        }
        
    }
    
    
    //下啦图片拉伸效果
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        if offsetY < 0 {
            let totalOffset = kMyHeaderViewHeight + abs(offsetY)
            let f = totalOffset / kMyHeaderViewHeight
            headerView.bgImageView.frame = CGRect(x: -screenWidth * (f - 1) * 0.5, y: offsetY, width: screenWidth * f, height: totalOffset)
        }
    }
    
}
