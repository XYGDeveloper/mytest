//
//  MyTabbar.swift
//  MyNewApp
//
//  Created by xyg on 2021/7/2.
//

import UIKit

class MyTabbar: UITabBar {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(publishButton)
        
        //设置主题
        theme_tintColor = "colors.tabbarTintColor"
        theme_barTintColor = "colors.cellBackgroundColor"
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    lazy var publishButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setBackgroundImage(UIImage(named: "feed_publish_44x44_"), for: .normal)
        button.setBackgroundImage(UIImage(named: "feed_publish_close_night_44x44_"), for: .selected)
        button.sizeToFit()
        return button
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let Width = frame.width
        //适配iphoneX
        let Height:CGFloat = 49
        
        //publishbutton坐标
        publishButton.center = CGPoint(x: Width * 0.5, y: Height * 0.5 - 7)
        
        //设置每个按钮的宽和高
        let ButtonWidth:CGFloat = Width * 0.2
        let ButtonHeight:CGFloat = Height
        let ButtonY = 0
        
        var index = 0
        
//        for (index,button) in subviews.enumerated() {
//            if !button.isKind(of: NSClassFromString("UITabBarButton")!) {
//                continue
//            }
//            let buttonX = ButtonWidth * (index > 1 ? CGFloat(index + 1):CGFloat(index))
//            button.frame = CGRect(x: buttonX, y: CGFloat(ButtonY), width: ButtonWidth, height: ButtonHeight)
//        }
        
        for button in subviews {
            
            if !button.isKind(of: NSClassFromString("UITabBarButton")!) {continue}
            
            let buttonX = ButtonWidth * (index > 1 ? CGFloat(index + 1):CGFloat(index))
            button.frame = CGRect(x: buttonX, y: CGFloat(ButtonY), width: ButtonWidth, height: ButtonHeight)
            index += 1
        }
        
    }
    
    
}
