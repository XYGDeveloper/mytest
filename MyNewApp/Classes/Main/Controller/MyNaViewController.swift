//
//  MyNaViewController.swift
//  MyNewApp
//
//  Created by xyg on 2021/7/2.
//

import UIKit

class MyNaViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        //修改navigationbar颜色
        let navigationBar = UINavigationBar .appearance()
        navigationBar.theme_tintColor = "colors.tabbarColor"
        navigationBar.theme_barTintColor = "colors.tabbarColor"

        // Do any additional setup after loading the view.
    }
    
    
    //navigation左侧返回按钮定制和隐藏底部bar
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        if viewControllers.count > 0 {
            viewController.hidesBottomBarWhenPushed = true
            viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "lefterbackicon_titlebar_night_24x24_"), style: .plain, target: self, action: #selector(back))
        }
        super.pushViewController(viewController, animated: true)

    }
    
    @objc func back() {
        popViewController(animated: true)
    }
    
}
