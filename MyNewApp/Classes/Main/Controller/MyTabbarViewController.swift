//
//  MyTabbarViewController.swift
//  MyNewApp
//
//  Created by xyg on 2021/7/2.
//

import UIKit

class MyTabbarViewController: UITabBarController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        print(tabBar.subviews)
        
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        //修改tabbar颜色
//        let tabbar = UITabBar.appearance()
//        tabbar.tintColor = UIColor.red
        
//        tabbar.tintColor = UIColor(red: <#T##CGFloat#>, green: <#T##CGFloat#>, blue: <#T##CGFloat#>, alpha: <#T##CGFloat#>)
        addController()
        
        //接收主题改变的通知
        
        NotificationCenter.default.addObserver(self, selector: #selector(receiveDayOrNightButtonClicked), name: NSNotification.Name(rawValue: "dayOrnightClick"), object: nil)
        
    }
    
    
    @objc func receiveDayOrNightButtonClicked(notification:NSNotification) {
        let isselected = notification.object as! Bool
        if isselected {
            //设置为夜间
            for childController in children {
                switch childController.title! {
                case "首页":
                    setNightViewcontroller(childController, image: "home")
                case "视频":
                    setNightViewcontroller(childController, image: "video")
                case "小视频":
                    setNightViewcontroller(childController, image: "huoshan")
                case "我的":
                    setNightViewcontroller(childController, image: "mine")
                default:
                    break
                }
            }
        }else{
            //设置为日间
            for childController in children {
                switch childController.title {
                case "首页":
                    setDayViewcontroller(childController, image: "home")
                case "视频":
                    setDayViewcontroller(childController, image: "video")
                case "小视频":
                    setDayViewcontroller(childController, image: "huoshan")
                case "我的":
                    setDayViewcontroller(childController, image: "mine")
                default:
                    break
                }
            }
        }
    }
    
    
    
//    func addController() {
//        setViewController(HomeViewController(), title: "首页", normalimage: "home_tabbar_32x32_", selectedImage: "home_tabbar_press_32x32_")
//        setViewController(VideoViewController(), title: "视频", normalimage: "video_tabbar_32x32_", selectedImage: "video_tabbar_press_32x32_")
//        setViewController(HuoshanViewController(), title: "小视频", normalimage: "huoshan_tabbar_32x32_", selectedImage: "huoshan_tabbar_press_32x32_")
//        setViewController(MineViewController(), title: "我的", normalimage: "mine_tabbar_32x32_", selectedImage: "mine_tabbar_press_32x32_")
//        //uitabbar是私有属性，是只读的，所以选择使用KVC
//        setValue(MyTabbar(), forKey: "tabBar")
//
//    }
    
//     func setViewController(
//        _ viewcontroller: UIViewController,
//        title:String,
//        normalimage:String,
//        selectedImage:String) {
//        //tabbar标题不显示
////        viewcontroller.tabBarItem.title = title
//        viewcontroller.title = title
//        viewcontroller.tabBarItem.image = UIImage(named: normalimage)
//        viewcontroller.tabBarItem.selectedImage = UIImage(named: selectedImage)
//        let navi = MyNaViewController(rootViewController: viewcontroller)
//        addChild(navi)
//    }
    
     func addController() {
        setViewController(HomeViewController(), title: "首页", image: "home")
        setViewController(VideoViewController(), title: "视频", image: "video")
        setViewController(HuoshanViewController(), title: "小视频", image:"huoshan")
        setViewController(MineViewController(), title: "我的", image: "mine")
        //uitabbar是私有属性，是只读的，所以选择使用KVC
        setValue(MyTabbar(), forKey: "tabBar")

    }
    
     func setViewController(
       _ viewcontroller: UIViewController,
       title:String,
       image:String
       ){
        if UserDefaults.standard.bool(forKey: isNight) {
            setNightViewcontroller(viewcontroller, image: image)
        }else{
            setDayViewcontroller(viewcontroller, image: image)
        }
       viewcontroller.title = title
       let navi = MyNaViewController(rootViewController: viewcontroller)
       addChild(navi)
   }
    
     func setNightViewcontroller(_ viewController:UIViewController,image:String) {
        viewController.tabBarItem.image = UIImage(named: image + "_tabbar_night_32x32_")
        viewController.tabBarItem.selectedImage = UIImage(named: image + "_tabbar_press_night_32x32_")
    }
    
     func setDayViewcontroller(_ viewController:UIViewController,image:String) {
        viewController.tabBarItem.image = UIImage(named: image + "_tabbar_32x32_")
        viewController.tabBarItem.selectedImage = UIImage(named: image + "_tabbar_press_32x32_")
    }
    


}
