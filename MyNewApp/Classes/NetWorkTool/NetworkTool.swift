//
//  NetworkTool.swift
//  MyNewApp
//
//  Created by xyg on 2021/7/3.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol NetworkToolProtocol {
    
    static func loadMycellData(completeHandle:@escaping(_ sections:[[MyCellModel]])->())
    
    static func loadMyconcernData(completeHandle:@escaping(_ concerns:[MyConcern])->())
    
}

extension NetworkToolProtocol{
    
    static func loadMycellData(completeHandle:@escaping(_ sections:[[MyCellModel]])->()){
        let url = BASE_URL + "/user/tab/tabs/?"
        let para = ["device_id":device_id]
        Alamofire.request(url,parameters: para).responseJSON { (response) in
            guard response.result.isSuccess else {return}
            if let value = response.result.value{
                let json = JSON(value)
//                print(json)
                guard json["message"] == "success" else { return }
                if let data = json["data"].dictionary {
                    if let sections = data["sections"]?.array {
                        var sectionArr = [[MyCellModel]]()
                        for item in sections {
                            var rows = [MyCellModel]()
                            for row in item.arrayObject! {
                                let myCellmodel = MyCellModel.deserialize(from: row as? NSDictionary)
                                rows.append(myCellmodel!)
                            }
                            sectionArr.append(rows)
                        }
                        completeHandle(sectionArr)
                    }
                }
            }
        }
        
    }
    
    static func loadMyconcernData(completeHandle:@escaping(_ concerns:[MyConcern])->()){
        let url = BASE_URL + "/concern/v2/follow/my_follow/?"
        let para = ["device_id":device_id]
        Alamofire.request(url,parameters: para).responseJSON { (response) in
            guard response.result.isSuccess else {return}
            if let value = response.result.value{
                let json = JSON(value)
                if let data = json["data"].arrayObject {
                    var myconcerns = [MyConcern]()
                    for item in data {
                        let myconcern = MyConcern.deserialize(from: item as! NSDictionary)
                        myconcerns.append(myconcern!)
                    }
                    completeHandle(myconcerns)
                }
                
            }
        }
    }
}


struct NetWorkTool:NetworkToolProtocol {
    
}
