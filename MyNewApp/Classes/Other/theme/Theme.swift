//
//  Theme.swift
//  MyNewApp
//
//  Created by xyg on 2021/7/3.
//

import Foundation
import SwiftTheme
enum Mytheme:Int {
    case day = 0
    case night = 1
    static var before = Mytheme.day
    static var current = Mytheme.night
    
    ///选择主题
    static func switchTo(_ theme:Mytheme){
        before = current
        current = theme
        switch theme {
        case .day:
            ThemeManager.setTheme(plistName: "default_theme", path: .mainBundle)
        case .night:
            ThemeManager.setTheme(plistName: "night_theme", path: .mainBundle)
        }
    }
    
    ///选择了夜间主题主题
    static func switchToNight(_ isNight:Bool){
        return switchTo(isNight ? .night:.day)
    }
    
    ///判断当前是否为夜间主题
    static func isNight() -> Bool {
        return current == .night
    }
    
}
